package com.optiim.automation;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.Assert;

import com.automation.data.ITestController;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.excel.ExcelDataReader;
import com.excel.ExcelTestListener;
import com.excel.UserCredentials;
import com.test.TestController;

import io.github.bonigarcia.wdm.WebDriverManager;

@Listeners(ExcelTestListener.class)
public class TestScenario implements ITestController{
	
	    private WebDriver driver;
	    private TestController testController;
	    private ExtentReports extentReports;
	    private ExtentTest extentTest;
	    private UserCredentials userCredentials;
	 	 
	 @BeforeSuite
	    void setUp() {
	    	WebDriverManager.chromedriver().setup();
	        driver = new ChromeDriver();
	        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	        testController = new TestController(driver);
	        driver.manage().window().maximize();
	       
	        
	        ExtentSparkReporter spark = new ExtentSparkReporter("Sparkreport.html");
	        extentReports = new ExtentReports();
	        extentReports.attachReporter(spark);
	       
	        ExcelDataReader excelDataReader = new ExcelDataReader(EXCEL_FILE_PATH);//BONUS
	        File excelFile = new File(EXCEL_FILE_PATH); 
	        if (!excelFile.exists()) {//user-credentials.xlsx dosyası yoksa oluşturup default giriş bilgileri setlenir.
	        	excelDataReader.createUserCredentialsExcelFile(excelFile);
	        }
	        List<UserCredentials> userCredentialsList = excelDataReader.readDataFromExcel(); //oluşturulan yada varolan dosyadan giriş bilgileri alınır.
	        userCredentials = userCredentialsList.get(0);//tek bir kimlik bilgisi oldugu için.
	    }

	    @Test(priority = 1, alwaysRun = false,enabled = true) 
	    void testOpenMainPageAndVerify() {
	    	extentTest = extentReports.createTest("Test: VatanBilgisayar","Includes functional testing.");
	    	extentTest.log(Status.INFO, "testOpenMainPageAndVerify() starting...");
	    	testController.open(URL);//sayfayı aç
	    	boolean isPageOpened = testController.isPageOpened(URL);//sayfanın açıldığını doğrula
	    	Assert.assertEquals(isPageOpened, true);
	    	if (isPageOpened) {
	            extentTest.log(Status.INFO, "Main page opened.");
    			extentTest.pass("testOpenMainPageAndVerify() is successful.");
	        } else {
	            extentTest.log(Status.INFO, "Main page could not opened.");
	            extentTest.fail("testOpenMainPageAndVerify() failed.");
	        }
	    }
	    @Test(priority = 2,dependsOnMethods = "testOpenMainPageAndVerify",enabled = true)
	    void testLogin() {
	    	extentTest.log(Status.INFO, "testLogin() starting...");
	    	testController.clickLoginButton(); //GİRİŞ YAP BUTONU  cssSelector ile 4.663  süre
	    	extentTest.log(Status.INFO, "Click the SIGN IN button.");
	    	testController.clickLoginText(); //Giriş Yap texti   xpath ile 4.629 süre
	    	extentTest.log(Status.INFO, "Clicked on Sign In.");
	        testController.login(userCredentials);//login olur.
	        extentTest.log(Status.INFO, "Login completed successful.");
	        extentTest.pass("testLogin() is successful.");
	    }
	    
	    @Test(priority = 3, dataProvider = "searchData",dependsOnMethods = "testLogin", enabled = true)
	    void testSearchAndVerify(String searchTerm) {
	    	extentTest.log(Status.INFO, "testSearchAndVerify() starting...");
	    	testController.searchProduct(searchTerm);//Samsung search eder.
	    	extentTest.pass("testSearchAndVerify() is successful.");
	    }
	    
	    @Test(priority = 4,dependsOnMethods = "testSearchAndVerify", enabled = true)
	    void testClickMobilePhoneCategory() {
	    	extentTest.log(Status.INFO, "testClickMobilePhoneCategory() starting...");
	    	testController.clickMobilePhoneCategory();// soldaki menüden Cep Telefonu kutusuna tıklar
	    	extentTest.log(Status.INFO, "Click on the Mobile Phone box from the left menu.");
	    	extentTest.pass("testClickMobilePhoneCategory() is successful.");
	    }
	    
	    @Test(priority = 5,dataProvider = "searchData",dependsOnMethods = "testClickMobilePhoneCategory",enabled = true)
	    void testSearchedTermVerify(String searchTerm) {
	    	
	    	boolean isSearchedTermPage = testController.isDisplayedForSearchedTerm(searchTerm);
	    	Assert.assertEquals(isSearchedTermPage, true);
	    	if(isSearchedTermPage) {
	    		extentTest.log(Status.INFO, "The page for the search term has been displayed.");
    			extentTest.pass("testSearchedTermVerify() is successful.");
	    	}else {
	    		extentTest.log(Status.INFO, "The page for the search term could not be displayed....Please check.");
	            extentTest.fail("testSearchedTermVerify()  failed.");
	    	}	    	
	    }
	    
	    @Test(priority = 6,dependsOnMethods = "testSearchedTermVerify",enabled = true) 
	    void testClickNextPageAndVerify() {
	    	extentTest.log(Status.INFO, "testClickNextPageAndVerify() starting...");
	    	testController.clickNextPageButton();//2.sayfaya tıklayan metot
	        boolean isPageActive = testController.isSecondPageActive(); //2.sayfada oldugunu kontrol eden metot
	        Assert.assertEquals(isPageActive, true);
	        if (isPageActive) {
	            extentTest.log(Status.INFO, "The second page is active.");
    			extentTest.pass("testClickNextPageAndVerify() is successful.");
	        } else {
	            extentTest.log(Status.INFO, "The second page is not active..Please check.");
	            extentTest.fail("testClickNextPageAndVerify() failed.");
	        }
	    }
	    
	    @Test(priority = 7,dependsOnMethods = "testClickNextPageAndVerify",enabled = true)
	    void testClickFiftyProduct() {
	    	extentTest.log(Status.INFO, "testClickFiftyProduct() starting...");
	    	testController.clickFifthProduct(); // Üstten 5.producta tıklayan metot
	    	extentTest.pass("testClickFiftyProduct() is successful.");
	    }
	    
	    @Test(priority = 8,dependsOnMethods = "testClickFiftyProduct",enabled = true)
	    void testClickAddFavoritesButton() { //Favoriye Ekle Butonuna tıklar
	    	extentTest.log(Status.INFO, "testClickAddFavoritesButton() starting...");
	    	testController.clickAddFavoritesButton();
	    	extentTest.pass("testClickAddFavoritesButton() is successful.");
	    }
	    
	    @Test(priority = 9,dependsOnMethods = "testClickAddFavoritesButton",enabled = true)
	    void testCheckPopupAddedFavorites() { //Favoriye Eklendi popup kontrolu
	    	extentTest.log(Status.INFO, "testCheckPopupAddedFavorites() starting...");
	    	boolean isClosePopup = testController.checkPopupAddedFavorites();
	    	Assert.assertEquals(isClosePopup, true);
	    	if(isClosePopup) {
	    		extentTest.log(Status.INFO, "Added to favorites popup closed.");
    			extentTest.pass("testCheckPopupAddedFavorites() is successful.");
	    	}else {
	    		extentTest.log(Status.INFO, "Added to favorites popup did not close..Please check.");
	            extentTest.fail("testCheckPopupAddedFavorites() failed.");
	    	}
	    }
	    
	    @Test(priority = 10,dependsOnMethods = "testCheckPopupAddedFavorites",enabled = true)
	    void testClickMyAccountAndFavoriteProducts() {// Hesabım => Favori Ürünlerim sayfasına gider.
	    	extentTest.log(Status.INFO, "testClickMyAccountAndFavoriteProducts() starting...");
	    	testController.clickMyAccountButton(); //Hesabım butonuna tıklar
	    	extentTest.log(Status.INFO, "Clicked my account button.");
	        testController.clickFavoritesProductsText(); //Hesabım menüsünden Favori Ürünlerim text ine tıklar
	        extentTest.log(Status.INFO, "Clicked favorite product text.");
	        extentTest.pass("testClickMyAccountAndFavoriteProducts() is successful.");
	    }

	    @Test(priority = 11,dependsOnMethods = "testClickMyAccountAndFavoriteProducts",enabled = true)
	    void testProductVerify() {
	    	//Açılan sayfada bir önceki sayfada beğendiklerime alınmış ürünün bulunduğunu onaylayacak
	    	
	    	//favoriye eklenen product ın ismini al, favori ürünlerim sayfasındaki ürün ismi ile aynı mı kontrol et.
	    }
	    
	    @Test(priority = 12,dependsOnMethods = "testProductVerify",enabled = true)
	    void testClickAddBasketText() {
	    	extentTest.log(Status.INFO, "testClickAddBasketText() starting...");
	    	testController.clickAddBasketText();//sepete ekleye tıklar
	    	extentTest.log(Status.INFO, "Clicked add basket text.");
	    	extentTest.pass("testClickAddBasketText() is successful.");
	    }
	    
	    @Test(priority = 13,dependsOnMethods = "testClickAddBasketText",enabled = true)
	    void testClosePopupAddBasket() {
	    	extentTest.log(Status.INFO, "testClosePopupAddBasket() starting...");
	    	testController.clickClosePopupAddBasket();// Sepete eklendi yazılı popup kontrolu yapılıp kapatılır 
	    	testController.clickRemoveProductFromFavoritesProducts(); //Sepete eklendikten sonra ürün hala favoride kalmaktaydı.Sepete eklenen ürün çıkarıldı.
	    	extentTest.pass("testClosePopupAddBasket() is successful.");
	    }
	    
	    @Test(priority = 14,dependsOnMethods = "testClosePopupAddBasket",enabled = true)
	    void testClickMyBasketButton() {
	    	extentTest.log(Status.INFO, "testClickMyBasketButton() starting...");
	    	testController.clickMyBasketButton();//Sepetim butonuna tıklar
	    	extentTest.pass("testClickMyBasketButton() is successful.");
	    }
	    
	    @Test(priority = 15,dependsOnMethods = "testClickMyBasketButton",enabled = true)
	    void testTrashBasketText() {
	    	extentTest.log(Status.INFO, "testTrashBasketText() starting...");
	    	testController.clickTrashBasketText(); // Sepeti Boşalt textine tıklar
	    	extentTest.pass("testTrashBasketText() is successful.");
	    }
	    
	    @Test(priority = 16,dependsOnMethods = "testTrashBasketText",enabled = true)
	    void testIsEmptyBasket() {
	    	extentTest.log(Status.INFO, "testIsEmptyBasket() starting...");
	        boolean isEmptyBasket = testController.isBasketEmpty();
	        Assert.assertEquals(isEmptyBasket, true);
	        if(isEmptyBasket) {
	        	extentTest.log(Status.INFO, "The basket is empty.");
    			extentTest.pass("testIsEmptyBasket() is successful.");
	    	}else {
	    		extentTest.log(Status.INFO, "The basket is not empty..Please check.");
	            extentTest.fail("testIsEmptyBasket() failed.");
	    	}
	    }
	    
	    @DataProvider(name = "userCredentialsData")
	    public Object[][] testUserCredentialsData() {	    	
	        return new Object[][] {
	            { new UserCredentials(userCredentials.getUsername(), userCredentials.getPassword())}
	        };
	    }
	    
	    @DataProvider(name = "searchData")
	    public Object[][] testSearchData() {
	        return new Object[][] {
	            {"samsung"}
	        };
	    }
	    
	    @AfterMethod
	    public void afterMethod(ITestResult result) {
	        if (result.getStatus() == ITestResult.FAILURE) {
	            extentTest.fail("TEST FAILED: " + result.getTestClass() + "DESCRIPTION: "+ result.getThrowable());
	        } else if (result.getStatus() == ITestResult.SKIP) {
	        	extentTest.skip("TEST SKIPPED: " + result.getTestClass() + "DESCRIPTION: "+ result.getThrowable());
	        }else if(result.getStatus() == ITestResult.SUCCESS) {
	        	extentTest.pass("TEST PASSED: " + result.getTestClass());
	        }
	    }
	    
	    @AfterSuite
	    public void tearDown() {
	        if (driver != null) {
	            driver.quit();
	        }
	        extentReports.flush();
	    }
}