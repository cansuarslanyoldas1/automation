package com.excel;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.automation.data.ITestController;

public class ExcelDataReader implements ITestController {
	
	private String excelFilePath;

    public ExcelDataReader(String excelFilePath) {
        this.excelFilePath = excelFilePath;
    }

    public void createUserCredentialsExcelFile(File excelFile) {
    	try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet(FIRST_SHEET_NAME);

            Row headerRow = sheet.createRow(0);
            Cell emailHeaderCell = headerRow.createCell(0);
            Cell passwordHeaderCell = headerRow.createCell(1);

            emailHeaderCell.setCellValue(FIRST_HEADER);
            passwordHeaderCell.setCellValue(SECOND_HEADER);

            Row dataRow = sheet.createRow(1);
            Cell emailCell = dataRow.createCell(0);
            Cell passwordCell = dataRow.createCell(1);

            emailCell.setCellValue(DEFAULT_USERNAME); //default value
            passwordCell.setCellValue(DEFAULT_PASSWORD); //default value

            try (FileOutputStream outputStream = new FileOutputStream(excelFile)) {
                workbook.write(outputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    
    public List<UserCredentials> readDataFromExcel() {
    	
    	List<UserCredentials> userCredentialsList = new ArrayList<>();
        try (InputStream inputStream = new FileInputStream(excelFilePath);
             Workbook workbook = new XSSFWorkbook(inputStream)) {

            Sheet sheet = workbook.getSheetAt(0);
            //Iterator<Row> rowIterator = sheet.iterator(); //tek bir kimlik bilgisi olacak iterator şuanlık gereksiz
            
            Row row = sheet.getRow(1);
            Cell emailCell = row.getCell(0);
            Cell passwordCell = row.getCell(1);

            String email = emailCell.getStringCellValue();
            String password = passwordCell.getStringCellValue();
            userCredentialsList.add(new UserCredentials(email,password));
            
            return userCredentialsList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public void deleteExcelFile() {
        File excelFile = new File(excelFilePath);
        if (excelFile.exists()) {
            if (excelFile.delete()) {
                System.out.println("Excel file has been successfully deleted.");
            } else {
                System.out.println("An error occurred while trying to delete the Excel file.");
            }
        } else {
            System.out.println("Excel file was not found.");
        }
    }
}

