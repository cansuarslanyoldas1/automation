package com.excel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class ExcelTestListener extends TestListenerAdapter {
    private Workbook workbook;
    private Sheet sheet;
    private int rowCount = 0;

    @Override
    public void onStart(ITestContext testContext) {
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Test Results");
        Row headerRow = sheet.createRow(rowCount++);
        Cell headerCell = headerRow.createCell(0);
        headerCell.setCellValue("Test Name");
        headerCell = headerRow.createCell(1);
        headerCell.setCellValue("Status");
        headerCell = headerRow.createCell(2);
        headerCell.setCellValue("Fail Description");
        headerCell = headerRow.createCell(3);
        headerCell.setCellValue("Timestamp");
        
    }

    @Override
    public void onTestFailure(ITestResult result) {
        writeResult(result.getName(), "FAIL", result.getThrowable().toString());
        
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        writeResult(result.getName(), "PASS", "");
    }

    private void writeResult(String testName, String status, String failDescription) {
        Row row = sheet.createRow(rowCount++);
        Cell cell = row.createCell(0);
        cell.setCellValue(testName);
        cell = row.createCell(1);
        cell.setCellValue(status);
        cell = row.createCell(2);
        if(status.equalsIgnoreCase("FAIL")) {
        	cell.setCellValue(failDescription);
        }
        
        cell = row.createCell(3);
        cell.setCellValue(new Date().toString());
    }

    @Override
    public void onFinish(ITestContext testContext) {
        try (FileOutputStream outputStream = new FileOutputStream("test_results.xlsx")) {
            workbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
