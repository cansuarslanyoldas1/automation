package com.automation.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class BasePage {
	protected WebDriver driver;
	public abstract void open(String url);
	
    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    protected void clickElement(WebElement element) {
    	element.click();
    }
    protected void clickElement(By locator) {
        driver.findElement(locator).click();
    }

    protected WebElement findElement(By locator) {
        return driver.findElement(locator);
    }
    
    protected String findElementText(By locator) {
        return driver.findElement(locator).getText();
    }

    protected void clearAndTypeText(By locator, String text) {
        WebElement element = driver.findElement(locator);
        element.clear();
        element.sendKeys(text);
    }

    public List<WebElement> findElements(By locator) {
 	   return driver.findElements(locator); 
    }
}
