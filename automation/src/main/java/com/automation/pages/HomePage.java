package com.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open(String url) {
        
    }

    public boolean isProductDisplayed(String productName, By productLocator) {
        for (WebElement productTitle : driver.findElements(productLocator)) {
            if (productTitle.getText().toLowerCase().contains(productName.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public WebElement findSecondPage(By pageLocator) {
    	
    	try {
    		return findElement(pageLocator);//ACTIVE_PAGE_INDICATOR
    	} catch (Exception e) {
            return null;
        }
    }
    
    public boolean isPageActive(By pageLocator, String pageNumber) {
    	   	
    	if(findSecondPage(pageLocator) != null && findSecondPage(pageLocator).getText().equalsIgnoreCase(pageNumber)) {
    		 return true;
        } 
         return false;
      }  	
}
