package com.automation.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.data.FavoritesProducts;

public class FavoritesPage extends BasePage {
	   
	public FavoritesPage(WebDriver driver) {
		super(driver);
	}
	@Override
    public void open(String url) {
        
    }
	
    public boolean isClosePopup(String sourcePopupMessage,By expectedMsgLocator, By buttonLocator) {
    	WebDriverWait wait = new WebDriverWait(driver, 10); 
    	if (wait.until(ExpectedConditions.presenceOfElementLocated(expectedMsgLocator)).getText().equalsIgnoreCase(sourcePopupMessage)) {
    		clickElement(buttonLocator);
    		return true;
        } else {
        	 return false;
        }   
    }

	public FavoritesProducts getFavoritesProductText(By locator) {
		FavoritesProducts products = new FavoritesProducts();
		products.setProductName(findElementText(locator));
		return products;
	}
    
   public void checkProductForFavPage(By favProductNameLocator,By productListOnFavPageLocator ) { //test11 için
	   FavoritesProducts products = getFavoritesProductText(favProductNameLocator); //PRODUCT_NAME_CLASSNAME
	   List<WebElement> productListOnFavPage = findElements(productListOnFavPageLocator);
	   for (WebElement productOnFavPage : productListOnFavPage) {
		   if(products.getProductName().equalsIgnoreCase(productOnFavPage.getText())) {
			   System.out.println("The product has been confirmed.");
		   }else {
			   System.out.println("It is not the same product.");
		   }
       }
   }
}
