package com.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.excel.UserCredentials;

public class LoginPage extends BasePage{
	
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open(String url) {
        driver.get(url);
    }
    public boolean isPageOpened(String url) {
        String currentUrl = driver.getCurrentUrl();
        return currentUrl.equalsIgnoreCase(url);
    }
    public void login(UserCredentials userCredentials,By loginUsernameIdLocator ,By loginPasswordIdLocator,By loginButtonIdLocator) {
        clearAndTypeText(loginUsernameIdLocator,userCredentials.getUsername()); 
        clearAndTypeText(loginPasswordIdLocator,userCredentials.getPassword()); 
        clickElement(loginButtonIdLocator);
    }
}
