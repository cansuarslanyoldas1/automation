package com.automation.data;

import org.openqa.selenium.By;

public class ConfigConstants {

	public enum GlobalConstants {
	    EXCEL_FILE_PATH("user-credentials.xlsx");

	    private final String value;

	    GlobalConstants(String value) {
	        this.value = value;
	    }

	    public String getValue() {
	        return value;
	    }
	}
	
	
	public enum VatanBilgisayarConstants {
	    URL("https://www.vatanbilgisayar.com/"),
		USERNAME("optiim.automation.aday@gmail.com"),
		PASSWORD("123456O.");
		
	    private final String value;

	    VatanBilgisayarConstants(String value) {
	        this.value = value;
	    }

	    public String getValue() {
	        return value;
	    }
	}
	
	public enum NewUserCredentialsFileConstants {
	    FIRST_SHEET_NAME("UserCredentials"),
	    FIRST_HEADER("USERNAME"),
		SECOND_HEADER("PASSWORD"),
		DEFAULT_USERNAME("optiim.automation.aday@gmail.com"),
		DEFAULT_PASSWORD("123456O.");
		
	    private final String value;

	    NewUserCredentialsFileConstants(String value) {
	        this.value = value;
	    }

	    public String getValue() {
	        return value;
	    }
	}
	
	public enum LoginPageConstants {
	    LOGIN_URL_ID(By.id("btnMyAccount")), 
	    LOGIN_BUTTON(By.xpath("//a[contains(text(), 'Giriş Yap')]")), 
	    LOGIN_USERNAME_ID(By.id("email")),
	    LOGIN_PASS_ID(By.id("pass")),
	    LOGIN_BUTTON_ID(By.id("login-button"));
		
		private final By by;

		LoginPageConstants(By by) {
	        this.by = by;
	    }

	    public By getBy() {
	        return by;
	    }
	}
	
	public enum FavoritesPageConstants {
	    FAVPAGE_BUTTON_ID(By.id("btnMyAccount")),
	    FAVPRODUCT_TEXT_ID(By.linkText("Favori Ürünlerim")),
	    FAV_ICON_ID(By.id("fav_Icon")),
	    EXPECTED_ADDED_FAVPAGE_POPUP_TEXT(By.xpath("//p[contains(@class, 'title') and text()='İlginize Teşekkür Ederiz']")),
		POPUP_CLOSE_BUTTON_CLASS(By.xpath("//button[@class='fancybox-button fancybox-close-small']")),
		ICON_TRASH_BUTTON(By.xpath("//a[contains(@class, 'basket-cart__footer-link') and contains(., 'Sepeti Boşalt')]")),
		MYBASKET_BUTTON_ID(By.id("btnMyBasket")),
		ADD_BASKET_TEXT(By.linkText("Sepete Ekle")),
		REMOVE_PRODUCT_ID(By.xpath("//a[contains(@class, 'basket-cart__product-remove')]")),
		EMPYT_BASKET_CHECK(By.className("empty-basket"));
		private final By by;

		FavoritesPageConstants(By by) {
	        this.by = by;
	    }

	    public By getBy() {
	        return by;
	    }
	}

	public enum MessageConstants{
		FAVPAGE_POPUP_MESSAGE("İlginize Teşekkür Ederiz"),
		PAGE_NUMBER("2"),
		EMPTY_BASKET_MESSAGE_CHECK("SEPETİNİZDE ÜRÜN YOK!");
		
		private final String value;

		MessageConstants(String value) {
	       this.value = value;
	    }

	    public String getValue() {
	        return value;
	    }
	}
	
	public enum HomePageConstants {
		SEARCH_ID(By.id("navbar-search-input")),
		SEARCH_BUTTON_ID(By.className("icon-search")),
		CATEGORY_MOBILE_PHONE(By.xpath("//span[contains(@class, 'filter-list__text') and contains(text(), 'Cep Telefonu')]")),
		NEXT_PAGE_BUTTON(By.xpath("//li[@class='pagination__item']/a[contains(@href, 'page=2')]")),
		ACTIVE_PAGE_INDICATOR(By.xpath("//li[@class='pagination__item active']")),  
		PRODUCT_LINKS(By.className("product-list__link")),
		SEARCHED_TEXT_ID(By.id("suggestionSearchİtem"));
		private final By by;

		HomePageConstants(By by) {
	        this.by = by;
	    }

	    public By getBy() {
	        return by;
	    }
	}
	

}
