package com.automation.data;

import org.openqa.selenium.By;

public interface ITestController {

	//VatanBilgisayarConstants
	 public static final String URL 		= ConfigConstants.VatanBilgisayarConstants.URL.getValue();
	
	//GlobalConstants
	 public static final String EXCEL_FILE_PATH = ConfigConstants.GlobalConstants.EXCEL_FILE_PATH.getValue();
	 
	//LoginPageConstants
	 public static final By LOGIN_URL_ID  	= ConfigConstants.LoginPageConstants.LOGIN_URL_ID.getBy();
	 public static final By LOGIN_BUTTON   	= ConfigConstants.LoginPageConstants.LOGIN_BUTTON.getBy();
	 public static final By LOGIN_USERNAME_ID  = ConfigConstants.LoginPageConstants.LOGIN_USERNAME_ID.getBy();
	 public static final By LOGIN_PASS_ID   = ConfigConstants.LoginPageConstants.LOGIN_PASS_ID.getBy();
	 public static final By BUTTON_LOGIN_ID = ConfigConstants.LoginPageConstants.LOGIN_BUTTON_ID.getBy();

	 //FavoritesPageConstants
	 public static final By POPUP_TEXT_ADDED_FAVPAGE = ConfigConstants.FavoritesPageConstants.EXPECTED_ADDED_FAVPAGE_POPUP_TEXT.getBy();
	 public static final By POPUP_CLOSE_BUTTON_CLASS = ConfigConstants.FavoritesPageConstants.POPUP_CLOSE_BUTTON_CLASS.getBy();
	 public static final By FAV_ICON_ID = ConfigConstants.FavoritesPageConstants.FAV_ICON_ID.getBy();
	 public static final By FAVPAGE_BUTTON_ID = ConfigConstants.FavoritesPageConstants.FAVPAGE_BUTTON_ID.getBy();
	 public static final By FAVPRODUCT_TEXT_ID = ConfigConstants.FavoritesPageConstants.FAVPRODUCT_TEXT_ID.getBy();
	 public static final By ADD_BASKET_TEXT = ConfigConstants.FavoritesPageConstants.ADD_BASKET_TEXT.getBy();
	 public static final By MYBASKET_BUTTON_ID = ConfigConstants.FavoritesPageConstants.MYBASKET_BUTTON_ID.getBy();
	 public static final By ICON_TRASH_BUTTON = ConfigConstants.FavoritesPageConstants.ICON_TRASH_BUTTON.getBy();
	 public static final By REMOVE_PRODUCT_ID = ConfigConstants.FavoritesPageConstants.REMOVE_PRODUCT_ID.getBy();
	 public static final By EMPYT_BASKET_CHECK = ConfigConstants.FavoritesPageConstants.EMPYT_BASKET_CHECK.getBy();
	 
	 //HomePageConstants
	 public static final By CATEGORY_MOBILE_PHONE = ConfigConstants.HomePageConstants.CATEGORY_MOBILE_PHONE.getBy();
	 public static final By SEARCH_ID = ConfigConstants.HomePageConstants.SEARCH_ID.getBy();
	 public static final By SEARCH_BUTTON_ID = ConfigConstants.HomePageConstants.SEARCH_BUTTON_ID.getBy();
	 public static final By ACTIVE_PAGE_INDICATOR = ConfigConstants.HomePageConstants.ACTIVE_PAGE_INDICATOR.getBy();
	 public static final By NEXT_PAGE_BUTTON = ConfigConstants.HomePageConstants.NEXT_PAGE_BUTTON.getBy();
	 public static final By PRODUCT_LINKS = ConfigConstants.HomePageConstants.PRODUCT_LINKS.getBy();
	 public static final By SEARCHED_TEXT_ID = ConfigConstants.HomePageConstants.SEARCHED_TEXT_ID.getBy();
	 
	 //NewUserCredentialsFileConstants
	 public static final String FIRST_SHEET_NAME = ConfigConstants.NewUserCredentialsFileConstants.FIRST_SHEET_NAME.getValue();
	 public static final String FIRST_HEADER = ConfigConstants.NewUserCredentialsFileConstants.FIRST_HEADER.getValue();
	 public static final String SECOND_HEADER = ConfigConstants.NewUserCredentialsFileConstants.SECOND_HEADER.getValue();
	 public static final String DEFAULT_USERNAME = ConfigConstants.NewUserCredentialsFileConstants.DEFAULT_USERNAME.getValue();
	 public static final String DEFAULT_PASSWORD = ConfigConstants.NewUserCredentialsFileConstants.DEFAULT_PASSWORD.getValue();
	 
	 //MessageConstants
	 public static final String FAVPAGE_POPUP_MESSAGE = ConfigConstants.MessageConstants.FAVPAGE_POPUP_MESSAGE.getValue();
	 public static final String PAGE_NUMBER = ConfigConstants.MessageConstants.PAGE_NUMBER.getValue();
	 public static final String EMPTY_BASKET_MESSAGE_CHECK = ConfigConstants.MessageConstants.EMPTY_BASKET_MESSAGE_CHECK.getValue();
}
