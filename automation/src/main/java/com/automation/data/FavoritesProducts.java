package com.automation.data;

public class FavoritesProducts {

	private String productName;
	private String productId;
	private String productTitle;
	
	public FavoritesProducts(String productName,String productId,String productTitle) {
		this.productName  = productName;
		this.productId	  = productId;
		this.productTitle = productTitle;
	}
	
	public FavoritesProducts() {
		
	}
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductTitle() {
		return productTitle;
	}
	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}
	
	
}
