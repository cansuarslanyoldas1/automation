package com.test;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.automation.data.ITestController;
import com.automation.pages.BasePage;
import com.automation.pages.FavoritesPage;
import com.automation.pages.HomePage;
import com.automation.pages.LoginPage;
import com.excel.UserCredentials;

public class TestController extends BasePage implements ITestController{
	
	private LoginPage loginPage = new LoginPage(driver);
	private HomePage homePage = new HomePage(driver);
	private FavoritesPage favPage = new FavoritesPage(driver);
	
	public TestController(WebDriver driver) {
		super(driver);
	}
	@Override
	public void open(String url) {
		loginPage.open(URL);
	}
	
	 public boolean isPageOpened(String url) {
		 return loginPage.isPageOpened(url);
	 }
	
	private void click(By locator) {	
		clickElement(locator); 
    }
	
	public void clickLoginButton() {	
		clickElement(LOGIN_URL_ID); 
    }
	
	public void clickLoginText() {
		clickElement(LOGIN_BUTTON);
	}
	public void clickMyAccountButton() {
		clickElement(FAVPAGE_BUTTON_ID);
	}
	public void clickFavoritesProductsText() {
		click(FAVPRODUCT_TEXT_ID);
	}
	public void clickAddBasketText() {
		click(ADD_BASKET_TEXT);
	}
	public void clickClosePopupAddBasket() {
		click(POPUP_CLOSE_BUTTON_CLASS);
	}
	public void clickRemoveProductFromFavoritesProducts() {
		click(REMOVE_PRODUCT_ID);
	}
	public void clickMyBasketButton() {
		click(MYBASKET_BUTTON_ID);
	}
	public void clickTrashBasketText() {
		click(ICON_TRASH_BUTTON);
	}
	
	public void searchProduct(String searchMessage) {
		searchProduct(searchMessage,SEARCH_ID, SEARCH_BUTTON_ID);
	}
	public String getSearchedElementText() {
		return findElementText(SEARCHED_TEXT_ID);
	}
	public void clickMobilePhoneCategory() {
		click(CATEGORY_MOBILE_PHONE);
	}
	public void clickNextPageButton() {
		click(NEXT_PAGE_BUTTON);
	}
	public void clickAddFavoritesButton() {
		click(FAV_ICON_ID);
		
	}
	public void login(UserCredentials userCredentials) { 
		loginPage.login(userCredentials,LOGIN_USERNAME_ID,LOGIN_PASS_ID,BUTTON_LOGIN_ID );
	}
	
	public void searchProduct(String productName, By findElementLocator, By searchButtonLocator) {
        findElement(findElementLocator).sendKeys(productName);
        clickElement(searchButtonLocator);
    }
	
	public boolean isSearchedTerm(String productName) {
		return homePage.isProductDisplayed(productName,SEARCHED_TEXT_ID);
	}

	private boolean isPageActive(By pageLocator,String pageNumber) {
		return homePage.isPageActive(pageLocator,pageNumber);
	}
	public boolean isSecondPageActive() {
		return isPageActive(ACTIVE_PAGE_INDICATOR,PAGE_NUMBER);
	}
	
	private void clickFifthProduct(By productListLocator) {
	    List<WebElement> productElements = driver.findElements(productListLocator); //PRODUCT_LINKS
	    if (productElements.size() >= 5) {
	           WebElement fifthProductTitleElement = productElements.get(4).findElement(By.tagName("h3"));
	           clickElement(fifthProductTitleElement); 
	    } else {
	        throw new NoSuchElementException("5th product from top not found.");
	     }
	}
	
	public void clickFifthProduct() {
		clickFifthProduct(PRODUCT_LINKS);      
	}
	
	public boolean isBasketEmpty(){
		WebElement basketStatusElement = findElement(EMPYT_BASKET_CHECK);
		WebElement messageElement = basketStatusElement.findElement(By.tagName("h3"));
        String message = messageElement.getText();
        if (message.equalsIgnoreCase(EMPTY_BASKET_MESSAGE_CHECK)) {
     	  return true;
        } 
     return false;   
	}
	
	public boolean checkPopupAndClose(String sourcePopupMessage,By expectedMsgLocator, By buttonLocator) {
		return favPage.isClosePopup(sourcePopupMessage, expectedMsgLocator, buttonLocator);
	}
	
	public boolean checkPopupAddedFavorites() {
		if(findElement(POPUP_TEXT_ADDED_FAVPAGE).isDisplayed()) {
	       	 return checkPopupAndClose(FAVPAGE_POPUP_MESSAGE, POPUP_TEXT_ADDED_FAVPAGE, POPUP_CLOSE_BUTTON_CLASS);
		 }
		return false;
	}
	
	public boolean isDisplayedForSearchedTerm(String searchTerm) {
		
		List<WebElement> productElements = driver.findElements(PRODUCT_LINKS);
		for(WebElement products: productElements) {
			if(!products.getText().toLowerCase().contains(searchTerm.toLowerCase())) {
				return false;
			}
		}
		return true;
	}
		
}
